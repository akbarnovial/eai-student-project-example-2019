package com.adf.tugasakhir.model;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.sql.Date;

/**
 * Conference model.
 */

@Entity
@Table(name = "conference")
@Data
public class Conference {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "nama")
    private String nama;

    @NotNull
    @Column(name = "tanggal_mulai")
    private Date tanggalMulai;

    @NotNull
    @Column(name = "ruangan_dipakai")
    private String ruanganDipakai;

    @NotNull
    @Column(name = "abstrak")
    private String abstrak;

}